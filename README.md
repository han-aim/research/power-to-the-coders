# Power to the coders tooling<!-- omit in toc -->

- [1. Design](#1-design)
- [2. Prerequisites](#2-prerequisites)
  - [2.1. Rust variant](#21-rust-variant)
- [3. Research questions (Dutch)](#3-research-questions-dutch)
- [4. To install](#4-to-install)
- [5. Run](#5-run)
  - [5.1. Requirements on experimental conditions](#51-requirements-on-experimental-conditions)
- [6. Future work](#6-future-work)
  - [6.1. Improve reliability](#61-improve-reliability)
    - [6.1.1. Improve robustness of software](#611-improve-robustness-of-software)
      - [6.1.1.1. Async support](#6111-async-support)
      - [6.1.1.2. Clock](#6112-clock)
    - [6.1.2. Randomize order of traced calls within trial](#612-randomize-order-of-traced-calls-within-trial)
    - [6.1.3. Use a more reliable execution environment](#613-use-a-more-reliable-execution-environment)
      - [6.1.3.1. Explore Rust unikernels](#6131-explore-rust-unikernels)
      - [6.1.3.2. Explore Talos Linux Kubernetes worker node](#6132-explore-talos-linux-kubernetes-worker-node)
    - [6.1.4. Control CPU power states](#614-control-cpu-power-states)
    - [6.1.5. Consider impact of benchmarking right after intensive work, such as a build](#615-consider-impact-of-benchmarking-right-after-intensive-work-such-as-a-build)
  - [6.2. Improve validity](#62-improve-validity)
    - [6.2.1. Select tests](#621-select-tests)
    - [6.2.2. Calculate absolute energy value correctly](#622-calculate-absolute-energy-value-correctly)
    - [6.2.3. Check zero deltas](#623-check-zero-deltas)
  - [6.3. Improve functionality](#63-improve-functionality)
    - [6.3.1. Support various CPUs/microarchitectures/peripherals/device types](#631-support-various-cpusmicroarchitecturesperipheralsdevice-types)
    - [6.3.2. Define follow up process and tools](#632-define-follow-up-process-and-tools)
    - [6.3.3. Integrate with CI/CD](#633-integrate-with-cicd)
  - [6.4. Improve quality in general](#64-improve-quality-in-general)
    - [6.4.1. Reimplementation in Rust](#641-reimplementation-in-rust)
    - [6.4.2. Use lesser privileges](#642-use-lesser-privileges)

## 1. Design

Pttc helps software developers measure the impact of software evolution on energy efficiency.
Pttc measures energy consumption of software functions between revisions/versions/variants of a software product.

![Architecture](Architecture.drawio.svg)

_Fig. 1: C4 Diagram. Transparent components are planned but not yet realized at all._

Pttc is alpha quality software and not supported as OSS.

## 2. Prerequisites

- GNU/Linux on a bare metal system, with superuser access, dedicated to Pttc.
- Bpftrace build of April 11 2023, or after.
- sudo
- Python 3.11
- PDM
- Superuser privileges

### 2.1. Rust variant

1. Install a Rust stable toolchain: `rustup install stable`
1. Install a Rust nightly toolchain with the `rust-src` component: `rustup toolchain install nightly --component rust-src`
1. Install `bpf-linker`: `cargo install bpf-linker`

## 3. Research questions (Dutch)

1. Wat is de betrouwbaarheid van de methode bij async testfuncties?

1. Hoe bepalen we welke testfuncties de ontwikkelaars willen onderzoeken op energiezuinigheid?

1. Moet de test suite uitgebreid worden met meer, andersoortige tests voor het effectief onderzoeken op energiezuinigheid?

1. Hoe integreren we de implementatie van de methode binnen de CI-omgeving voor ntpd-rs?

1. Hoe integreren we de implementatie van de methode op de werkcomputers van ontwikkelaars voor ntpd-rs?

    `hostPath` mount.

1. Moet de test suite sequentieel uitgevoerd worden, of niet?

    Ja, en in willekeurige volgorde.

1. In hoeverre moeten en kunnen er historische meetgegevens worden bijgehouden?

    Gegevens komen eventueel in de CI cache.

1. Verder moet er nog een delta berekend worden en op een optimale manier gepresenteerd.
    Maar dat laatste hangt sterk samen met hoe je het integreert in de CI-omgeving van `ntpd-rs`, dus heeft maar beperkt zin voor mij om tijd in te steken op voorhand.

    Delta wordt berekend.
    Heatmap over experiment: meerdere trials.
    Experiment moet nog geautomatiseerd worden.
    De heatmap komt in een HTML-pagina.
    De inhoud daarvan is nu niet makkelijk te reviewen.
    In een MR-thread kan hooguit een comment komen met hyperlink naar de HTML-pagina.

## 4. To install

From the root directory of your working copy:

```sh
dotenv \
  run \
    kustomize \
      build \
        overlays/experiment > \
  /tmp/manifest.yml
kubeconform \
  -exit-on-error \
  -output pretty \
  -strict \
  /tmp/manifest.yml
dotenv \
  run \
    python3 \
      -m aimtools.k8s.create_namespace \
      pttc
kubectl \
  apply \
    --dry-run=server \
    --filename=/tmp/manifest.yml \
    --validate=strict
kubectl \
  apply \
    --dry-run=server \
    --filename=/tmp/manifest.yml \
    --validate=strict
kubectl \
  delete \
    --filename=/tmp/manifest.yml
```

## 5. Run

```sh
env \
  RUST_LOG=info \
  cargo \
    xtask \
      run
```

### 5.1. Requirements on experimental conditions

- The subject process is assigned to a specific CPU core.
- No other processes are assigned to that CPU core.
- The CPU core has a constant clock frequency.
  - The CPU's `cpufreq` governor is the Performance governor.
- The subject process is single-threaded.
- The subject function is synchronous.
- The system supports a `CLOCK_MONOTONIC_RAW` clock.
- The subject system stays in C0 power state.
- `constant_tsc`
- The subject process should terminate itself during the experiment (ideally timely).

Tip: disable C-states and P-states in the system's BIOS.

// TODO: https://www.reddit.com/r/rust/comments/twvz21/comment/i3pldii/
// https://github.com/ThijsRay/coppers?tab=readme-ov-file#accuracy

## 6. Future work

- Test run from within dev container.
- Test run on server.

### 6.1. Improve reliability

#### 6.1.1. Improve robustness of software

Experiments are pretty involved at this point.
It's also unverified that all calls are successfully intercepted and whether all logged interceptions are real.

##### 6.1.1.1. Async support

Some tests lead to unbalanced uprobe/uretprobe interceptions, and multiple even though the test function should be called only once.
This probably is down to usage of the Tokio async runtime (some test functions are annotated with an attribute as such).

##### 6.1.1.2. Clock

Use a clock that has a high resolution, low overhead, is monotonic and not subject to corrections (e.g., NTP) during experiments.
Now: bpftrace's nsecs since boot.
That seems appropriate, but perhaps not entirely.

#### 6.1.2. Randomize order of traced calls within trial

For example, randomize order of tests.

#### 6.1.3. Use a more reliable execution environment

Currently, we use an ODROID-H3 SBC with multi-user, multi-process, general purpose GNU/Linux.

##### 6.1.3.1. Explore Rust unikernels

For now, Unikraft supports VMs only (?), which is incompatible with hardware-related research such as energy efficiency research.

##### 6.1.3.2. Explore Talos Linux Kubernetes worker node

Kubernetes can be set up so that a worker node is dedicated to the experiment, and that hardware resource allocation and scheduling is tightly controlled.
Furthermore, Kubernetes is attractive as a standard and simple way to submit the test workloads and gather results.
Talos Linux adds a minimal Linux OS for e.g., the worker nodes.
A GNU/Linux distribution that can be extensively configured, including kernel (build) configuration.
Talos Linux also offers installation on bare metal and management of bare metal nodes (power-on, power-off), etc.

#### 6.1.4. Control CPU power states

#### 6.1.5. Consider impact of benchmarking right after intensive work, such as a build

### 6.2. Improve validity

#### 6.2.1. Select tests

https://nexte.st/book/test-groups.html

#### 6.2.2. Calculate absolute energy value correctly

https://github.com/amd/amd_energy#energy-caluclation

#### 6.2.3. Check zero deltas

Some RAPL energy usage deltas are zero.
Double check that’s is because of RAPL frequency and/or RAPL unit size.

### 6.3. Improve functionality

#### 6.3.1. Support various CPUs/microarchitectures/peripherals/device types

#### 6.3.2. Define follow up process and tools

Write workflows and functional requirements for follow-up on the results of this tool: a heatmap and data logs.

#### 6.3.3. Integrate with CI/CD

The output needs to be easily accessible from CI/CD.
Perform user research to gather UI requirements.

### 6.4. Improve quality in general

#### 6.4.1. Reimplementation in Rust

To gain a speedup.
To make development easier and more durable.
To simplify solution architecture, using a single programming language.

#### 6.4.2. Use lesser privileges

Use Linux capabilities rather than superuser privileges.
