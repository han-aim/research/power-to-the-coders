#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]
#![allow(clippy::missing_errors_doc)]

use std::{
    io,
    io::{
        stdout,
        Write,
    },
};

use io_uring::{
    cqueue,
    opcode,
    squeue,
    types::Fd,
    IoUring,
};
use nix::{
    fcntl::{
        open,
        OFlag,
    },
    sys::{
        stat::Mode,
        time::TimeSpec,
    },
    time::{
        clock_getres,
        clock_gettime,
        ClockId,
    },
    // unistd::close,
};
// use signal_hook::{
//     consts::signal::SIGINT,
//     iterator::Signals,
// };

const COMMA: &[u8] = b",";
const NEWLINE: &[u8] = b"\n";
const PATHS_SENSOR: [&str; 1] = [
    "/sys/devices/virtual/thermal/cooling_device0/power/runtime_status",
    // "/sys/devices/power/"
    // "/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj",
    // "/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/intel-rapl:0:2/energy_uj",
    // "/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/intel-rapl:0:0/energy_uj",
    // "/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/intel-rapl:0:1/energy_uj",
];

// TODO: One thread per sensor.

fn main() -> Result<(), io::Error> {
    dbg!(clock_getres(ClockId::CLOCK_MONOTONIC_RAW)?);
    dbg!(PATHS_SENSOR);
    let fds_sensor: [i32; PATHS_SENSOR.len()] = std::array::from_fn(|index| {
        open(
            PATHS_SENSOR[index],
            OFlag::O_RDONLY | OFlag::O_NOFOLLOW | OFlag::O_CLOEXEC,
            Mode::empty(),
        )
        .map_err(|fault| {
            eprintln!(
                "Failed to open sensor {} at path {}",
                index, PATHS_SENSOR[index]
            );
            fault
        })
        .unwrap()
    });
    dbg!(fds_sensor);
    // Close fds upon termination
    // let fds_data_2 = fds_data.clone();
    // thread::spawn(move || {
    //     for signal in signals.forever() {
    //         if let SIGINT = signal {
    //             eprintln!("Received signal {:?}", signal);
    //             for fd in fds_data_2.iter().take(PATHS_DATA.len()) {
    //                 let _ = close(*fd);
    //             }
    //         }
    //     }
    // });
    let len_entries_usize = fds_sensor.len().next_power_of_two();
    let len_entries: u32 = len_entries_usize.try_into().unwrap();
    let iouring = IoUring::builder()
        .setup_r_disabled()
        .setup_single_issuer()
        .setup_defer_taskrun()
        // TODO: fails
        // .setup_sqpoll(100)
        // .setup_sqpoll_cpu(1)
        .dontfork()
        .build(len_entries as _)
        .map_err(|fault| {
            eprintln!("Failed to create `io_uring`.");
            fault
        })?;
    stdout().write_all(b"tv_sec,tv_nssec,data,index_sensor\n")?;
    let mut buffer_integertostring = itoa::Buffer::new();
    // TODO: optimize buffer size
    let mut buffer_data: [[u8; 1024]; PATHS_SENSOR.len()] = [[0; 1024]; PATHS_SENSOR.len()];
    let mut timestamp: TimeSpec;
    let submitter = iouring.submitter();
    let entries_submission: [squeue::Entry; PATHS_SENSOR.len()] = {
        // Cleanup all fixed buffers (if any), then register one, with id 0.
        let _ = submitter.unregister_buffers();
        let iovecs: [libc::iovec; PATHS_SENSOR.len()] = std::array::from_fn(|index| libc::iovec {
            iov_base: std::ptr::addr_of_mut!(buffer_data[index]).cast::<libc::c_void>(),
            iov_len: buffer_data[index].len(),
        });
        unsafe { submitter.register_buffers(&iovecs).unwrap() };
        dbg!(iovecs);
        submitter.register_files(&fds_sensor)?;
        // TODO: performance impact?
        // submitter.register_restrictions(&mut [Restriction::sqe_op(opcode::ReadFixed::CODE)])?;
        submitter.register_enable_rings()?;
        std::array::from_fn(|index| -> squeue::Entry {
            opcode::ReadFixed::new(
                Fd(fds_sensor[index]),
                // TODO: iovecs.base?
                buffer_data[index].as_mut_ptr(),
                buffer_data[index].len().try_into().unwrap(),
                index.try_into().unwrap(), // TODO
            )
            .build()
            .user_data(index.try_into().unwrap())
        })
    };
    let mut iouring = iouring;
    let mut entry_completion: cqueue::Entry;
    let mut index_udata: usize;
    let mut bytes_to_read: usize;
    loop {
        unsafe {
            iouring
                .submission()
                .push_multiple(&entries_submission.to_owned())
                .expect("Submission queue is full.");
        }
        // To not only batch submissions, but also combine submissions and completions into a single syscall.
        iouring.submitter().submit_and_wait(len_entries_usize)?;
        entry_completion = iouring
            .completion()
            .next()
            .expect("Completion queue is empty.");
        assert!(
            entry_completion.result() >= 0,
            "Failed `ReadFixed` (errno: {})",
            entry_completion.result()
        );
        timestamp = clock_gettime(ClockId::CLOCK_MONOTONIC_RAW)?;
        // TODO: write output more efficiently.
        stdout().write_all(buffer_integertostring.format(timestamp.tv_sec()).as_bytes())?;
        stdout().write_all(COMMA)?;
        stdout().write_all(
            buffer_integertostring
                .format(timestamp.tv_nsec())
                .as_bytes(),
        )?;
        stdout().write_all(COMMA)?;
        if entry_completion.result() > 1 {
            // Strip newline.
            bytes_to_read =
                i32::try_into(entry_completion.result().checked_sub(1).unwrap()).unwrap();
            index_udata = entry_completion.user_data().try_into().unwrap();
            stdout().write_all(&buffer_data[index_udata][..bytes_to_read])?;
            stdout().write_all(COMMA)?;
            stdout().write_all(
                buffer_integertostring
                    .format(entry_completion.user_data())
                    .as_bytes(),
            )?;
            stdout().write_all(NEWLINE)?;
        } else {
            panic!("Failed to read sensor data.")
        };
    }
}
