#![no_std]
#![no_main]
#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]
#![allow(clippy::missing_errors_doc)]

use aya_bpf::{
    helpers::bpf_ktime_get_ns,
    macros::{
        map,
        uprobe,
        uretprobe,
    },
    maps::PerCpuArray,
    programs::ProbeContext,
};

#[map]
static FUNCTION_AND_TIMESTAMP_ENTRY_AND_TIMESTAMP_EXIT: PerCpuArray<u64> =
    PerCpuArray::<u64>::with_max_entries(2, 0);

fn log_timestamp(index: u32) -> u32 {
    if let Some(timestamp) = FUNCTION_AND_TIMESTAMP_ENTRY_AND_TIMESTAMP_EXIT.get_ptr_mut(index) {
        unsafe {
            *timestamp = bpf_ktime_get_ns();
        }
    } else {
        return 1;
    };

    return 0;
}

#[allow(clippy::needless_pass_by_value)]
#[uprobe]
pub fn start(_ctx: ProbeContext) -> u32 {
    log_timestamp(0)
}

#[allow(clippy::needless_pass_by_value)]
#[uretprobe]
pub fn finish(_ctx: ProbeContext) -> u32 {
    log_timestamp(1)
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
