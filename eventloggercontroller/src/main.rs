#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]
#![allow(clippy::missing_errors_doc)]

use std::{
    fs::File,
    io::Read,
    thread,
    time::Duration,
};

use aya::{
    maps::{
        PerCpuArray,
        PerCpuValues,
    },
    programs::UProbe,
    Bpf,
};
use aya_log::BpfLogger;
use clap::{
    arg,
    command,
    value_parser,
};
use libc::pid_t;
use log::{
    info,
    warn,
};
use serde::Deserialize;
use tokio::signal;

#[derive(Debug, Deserialize)]
struct Config {
    names_function: Vec<String>,
    paths_sensor: Vec<String>,
    path_executable: String,
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    env_logger::init();
    let options = command!()
        .arg(
            arg!(
                --pid [PID] "ID of process to probe"
            )
            .required(true)
            .value_parser(value_parser!(pid_t)),
        )
        .get_matches();
    let mut file = File::open("config.toml").expect("Failed to open config file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Failed to read file");
    let contents = contents;
    let config: Config =
        toml::from_str(contents.as_str()).expect("Failed to parse configuration file.");
    eprintln!("{:#?}", &config);
    let mut ebpf = Bpf::load_file(if cfg!(debug_assertions) {
        "eventlogger.dbg"
    } else {
        "eventlogger"
    })?;
    if let Err(fault) = BpfLogger::init(&mut ebpf) {
        // This can happen if you remove all log statements from your eBPF program.
        warn!("Failed to initialize eBPF logger: {}", fault);
    }
    let program_start: &mut UProbe = ebpf.program_mut("start").unwrap().try_into()?;
    program_start.load()?;
    let program_finish: &mut UProbe = ebpf.program_mut("finish").unwrap().try_into()?;
    program_finish.load()?;
    for name_function in config.names_function {
        for (name_program, program) in ebpf.programs_mut() {
            dbg!(name_program);
            let uprobe: &mut UProbe = TryFrom::try_from(program)?;
            uprobe.attach(
                Some(&name_function),
                0,
                config.path_executable.as_str(),
                options.get_one::<pid_t>("pid").copied(),
            )?;
        }
    }
    let pause = Duration::new(10, 0);
    thread::sleep(pause);
    let function_and_timestamp_entry_and_timestamp_exit =
        PerCpuArray::try_from(ebpf.map_mut("FUNCTION_AND_TIMESTAMP_ENTRY_AND_TIMESTAMP_EXIT")?)?;
    let timestamp_entry: PerCpuValues<u64> =
        function_and_timestamp_entry_and_timestamp_exit.get(&0, 0)?;
    let timestamp_exit: PerCpuValues<u64> =
        function_and_timestamp_entry_and_timestamp_exit.get(&1, 0)?;
    // TODO: iterate over results and write to file with io_uring.
    info!(
        "{},{},{}",
        ebpf.programs()
            .next()
            .expect("Cannot find eBPF program. ")
            .0,
        timestamp_entry[0],
        timestamp_exit[1]
    );
    info!("Waiting for SIGTERM/Ctrl-C ...");
    signal::ctrl_c().await?;
    info!("Exiting ...");
    Ok(())
}
