#!/bin/bash -eux
set -o pipefail
# shellcheck source=/dev/null
. '.venv/bin/activate'

cd ~/ntpd-rs/
# TODO:
cargo clean
cd -

run_trial() {
  cd ~/ntpd-rs/
  git checkout --detach --no-guess "${1:?}"
  python3 -m pttctool
  # TODO: Reset on fault
  git checkout -
  cd -
}

run_trial HEAD~5
run_trial HEAD~4
run_trial HEAD~3
run_trial HEAD~2
run_trial HEAD~1
run_trial HEAD

ls -lah --sort=time ~/ntpd-rs/target/eventloggercontroller
