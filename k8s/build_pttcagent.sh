#!/bin/bash -eux
set -o pipefail
sccache \
  --show-stats
rustc \
  --verbose \
  --version
export \
  -- \
  CARGO_TARGET_X86_64_UNKNOWN_LINUX_GNU_LINKER='/usr/bin/mold' \
  RUSTFLAGS='-C link-arg=-fuse-ld=mold'
rustup \
  target \
  add \
  "$1"
rustup \
  show
cargo \
  +nightly \
  miri \
  setup
cd \
  -- \
  eventlogger
# TODO: `RUSTFLAGS` should be sanitized for linker-args since `bpf-linker` won't support them, e.g. `-fuse=...`.
# This is a work-around.
env \
  -u RUSTUP_TOOLCHAIN \
  -u RUSTFLAGS \
  cargo \
  build \
  --locked \
  --release \
  --target=bpfel-unknown-none \
  -Z build-std=core
cd \
  -- \
  ..
cargo \
  build \
  --locked \
  --release \
  --target "$1" \
  --verbose
cd \
  -- \
  eventloggercontroller/
cargo \
  build \
  --locked \
  --release \
  --target "$1" \
  --verbose
cd \
  -- \
  -
cd \
  -- \
  energydatalogger/
cargo \
  build \
  --locked \
  --release \
  --target "$1" \
  --verbose
cd \
  -- \
  -
readelf \
  -p .comment \
  'target/release/eventloggercontroller'
[ -d "/opt/app/bin/$1" ] || install \
  -d \
  -- \
  "/opt/app/bin/$1"
cp \
  -p \
  -- \
  'target/release/eventloggercontroller' \
  'target/release/energydatalogger' \
  'target/bpfel-unknown-none/release/eventlogger' \
  '/opt/app/bin/'
