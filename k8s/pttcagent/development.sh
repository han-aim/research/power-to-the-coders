#!/bin/bash -eux
set -o pipefail
sccache \
  --show-stats
rustc \
  --verbose \
  --version
cd \
  -- \
  /builds
rustup \
  show
env \
  cargo \
  binstall \
  bpf-linker
env \
  RUSTFLAGS="${RUSTFLAGS:=} -C target-feature=-crt-static -L native=/usr/lib -lstdc++" \
  cargo \
  install \
  bpf-linker \
  --features llvm-sys/force-dynamic \
  --no-default-features \
  --locked \
  --verbose
