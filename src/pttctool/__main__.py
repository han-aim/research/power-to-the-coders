from __future__ import annotations

import json
import tomllib
from abc import ABC
from dataclasses import asdict, dataclass, is_dataclass
from json import JSONEncoder
from logging import INFO, basicConfig, getLogger
from os import chown, environ, fsync
from pathlib import Path
from pprint import pformat
from resource import RLIMIT_NOFILE, RLIM_INFINITY, getrlimit, setrlimit
from string import Template
from subprocess import DEVNULL, CalledProcessError, Popen, run
from tempfile import mkdtemp
from time import sleep
from typing import TYPE_CHECKING, Any, cast

# TODO: ambiguous
from pttctool.heatmap import heatmap

if TYPE_CHECKING:
    from typing import TypeVar

    T = TypeVar("T")

PATH_POWERCAP_RAPL = Path("/sys/devices/virtual/powercap/intel-rapl/")


@dataclass(frozen=True, kw_only=True, slots=True)
class Record:
    delta_energy: int
    delta_time: int


class DataclassJSONEncoder(JSONEncoder):
    def default(self, o: T) -> dict[str, Any]:
        return asdict(obj=o) if is_dataclass(obj=o) else cast(dict[str, Any], super().default(o=o))


@dataclass(frozen=True, kw_only=True, slots=True)
class Probe(ABC):
    records: dict[str, int]


class Uprobe(Probe):
    pass


class Uretprobe(Probe):
    pass


_LOGGER = getLogger(__name__)
FORMAT = "%(levelname)s %(asctime)s %(process)d %(module)s %(funcName)s() %(message)s"
basicConfig(level=INFO, format=FORMAT)


def _getsetlimits() -> None:
    _LOGGER.info("getrlimit before: %s", getrlimit(RLIMIT_NOFILE))
    try:
        setrlimit(RLIMIT_NOFILE, (RLIM_INFINITY, RLIM_INFINITY))
    except (OSError, ValueError):
        _LOGGER.exception("Failed to set rlimit. ")
    _LOGGER.info("getrlimit after: %s", getrlimit(RLIMIT_NOFILE))


# TODO: Make dynamic
def _template_bpftrace_header() -> str:
    return """print("types_record=['type_probe','crate','functionitempath','nsecs_before',");
    print("'intel-rapl:0/energy_uj',");
    print("'intel-rapl:0/intel-rapl:0:0/energy_uj',");
    print("'intel-rapl:1/energy_uj',");
    print("'nsecs_after']");
    print("values=[");"""


def _template_bpftrace_logger(
    type_probe: str,
    crate: str,
    functionitempath: str,
) -> str:
    return Template(
        """printf("['%s','%s','%s',%u,", "$type_probe", "$crate", "$functionitempath", nsecs);
    cat("intel-rapl:0/energy_uj");
    print(",");
    cat("intel-rapl:0/intel-rapl:0:0/energy_uj");
    print(",");
    cat("intel-rapl:1/energy_uj");
    printf(",%u],\\n", nsecs);""",
    ).substitute(
        {
            "crate": crate,
            "functionitempath": functionitempath,
            "type_probe": type_probe,
        },
    )


# TODO: validate identifiers https://www.mankier.com/8/bpftrace#Bpftrace_Language-Identifiers
def _template_bpftrace(
    path_executable: str,
    functionitempath: str,
    crate: str,
    returning: bool,
) -> str:
    return Template(
        # Perf: """tracepoint:probe_$crate:$shorthand_probe
        # // Traces $functionitempath in $path_executable
        """$type_probe:"$path_executable":"$crate::$functionitempath"
{
    $logger
}

""",
    ).substitute(
        {
            "crate": crate,
            "path_executable": path_executable,
            "logger": _template_bpftrace_logger(
                crate=crate,
                type_probe="uretprobe" if returning else "uprobe",
                functionitempath=functionitempath,
            ),
            "type_probe": "uretprobe" if returning else "uprobe",
            "functionitempath": functionitempath,
        },
    )


def _analyze(path_dir_pttc: Path, uid: int, gid: int) -> None:
    gitdescription = _get_experimentcondition_git(path_dir_pttc=path_dir_pttc, uid=uid, gid=gid)
    # TODO: infosec, normalize gitdescription
    path_dir_pttcrun = Path(mkdtemp(prefix=f"{gitdescription} ", dir=path_dir_pttc))
    _LOGGER.info("Starting analysis trial `%s` ...", path_dir_pttcrun.name)
    output_nextest = _run_nextest_list(uid=uid, gid=gid, path_dir_pttcrun=path_dir_pttcrun)
    path_trees, path_file_datalog = _generate_bpftrace(path_dir_pttcrun=path_dir_pttcrun, output_nextest=output_nextest)
    _run_bpftrace(uid, gid, path_dir_pttcrun, path_trees, path_file_datalog)
    datalog = {}
    with path_file_datalog.open("rb") as file_datalog:
        datalog = tomllib.load(file_datalog)
    _process_datalog(path_dir_pttcrun=path_dir_pttcrun, datalog=datalog)


def _process_datalog(path_dir_pttcrun: Path, datalog: dict) -> None:
    crate_to_functionitempath_to_uretprobes: dict[str, dict[str, list[Uretprobe]]] = {}
    crate_to_functionitempath_to_uprobes: dict[str, dict[str, list[Uprobe]]] = {}
    for entry in datalog["values"]:
        type_probe = str(entry[0])
        crate = str(entry[1])
        functionitempath = str(entry[2])
        records = dict(zip(datalog["types_record"][3:], entry[3:], strict=True))
        if type_probe == "uprobe":
            uprobe = Uprobe(
                records=records,
            )
            crate_to_functionitempath_to_uprobes.setdefault(crate, {}).setdefault(functionitempath, [uprobe]).append(
                uprobe,
            )
        elif type_probe == "uretprobe":
            uretprobe = Uretprobe(
                records=records,
            )
            crate_to_functionitempath_to_uretprobes.setdefault(crate, {}).setdefault(
                functionitempath,
                [uretprobe],
            ).append(uretprobe)
    crate_to_functionitempath_to_deltarecord: dict[str, dict[str, Record]] = {}
    for (
        crate,
        functionitempath_to_uretprobes,
    ) in crate_to_functionitempath_to_uretprobes.items():
        _LOGGER.debug("Processing crate %s", crate)
        for functionitempath, uretprobes in functionitempath_to_uretprobes.items():
            # TODO: Run only for single pairs uprobe,uretprobe rather than any number of pairs
            if len(crate_to_functionitempath_to_uprobes[crate][functionitempath]) == len(uretprobes):
                uprobe = crate_to_functionitempath_to_uprobes[crate][functionitempath].pop()
                uretprobe = uretprobes.pop()
                # TODO: solve this discrepancy
                try:
                    assert uretprobe.records["nsecs_after"] > uprobe.records["nsecs_before"]
                except AssertionError:
                    _LOGGER.exception(
                        "The finishing time of the uretprobe must be later than the starting time of the uprobe: %s",
                    )
                try:
                    # TODO: verify
                    assert uretprobe.records["intel-rapl:0/energy_uj"] >= uprobe.records["intel-rapl:0/energy_uj"]
                except AssertionError:
                    _LOGGER.exception("The energy consumed at the uretprobe must be >= than at the uprobe: %s")
                delta_time = uretprobe.records["nsecs_after"] - uprobe.records["nsecs_before"]
                delta_energy = uretprobe.records["intel-rapl:0/energy_uj"] - uprobe.records["intel-rapl:0/energy_uj"]
                # TODO: deal with duplicate values
                crate_to_functionitempath_to_deltarecord.setdefault(crate, {})[functionitempath] = Record(
                    delta_energy=delta_energy,
                    delta_time=delta_time,
                )
            else:
                _LOGGER.error("%s has unequal numbers of enters vs. returns", functionitempath)
    _LOGGER.debug(
        "functionitempath_to_uprobes: %s",
        pformat(crate_to_functionitempath_to_uprobes[crate]),
    )
    _LOGGER.debug(
        "functionitempath_to_uretprobes: %s",
        pformat(crate_to_functionitempath_to_uretprobes[crate]),
    )
    _LOGGER.debug(
        "functionitempath_to_delta_time_and_delta_energy: %s",
        pformat(crate_to_functionitempath_to_deltarecord[crate]),
    )
    path_file_crate_to_functionitempath_to_deltarecord = (
        path_dir_pttcrun / "crate_to_functionitempath_to_deltarecord.json"
    )
    with path_file_crate_to_functionitempath_to_deltarecord.open(
        "wt",
        encoding="utf-8",
    ) as file_crate_to_functionitempath_to_deltarecord:
        json.dump(
            cls=DataclassJSONEncoder,
            fp=file_crate_to_functionitempath_to_deltarecord,
            obj=crate_to_functionitempath_to_deltarecord,
            indent=1,
        )


def _get_experimentcondition_git(path_dir_pttc: Path, uid: int, gid: int) -> str:
    try:
        completedprocess_gitdescribe = run(
            ["git", "rev-parse", "--short", "HEAD"],  # noqa: S603 S607
            capture_output=True,
            check=True,
            cwd=path_dir_pttc,
            encoding="utf-8",
            group=gid,
            text=True,
            user=uid,
        )
    except CalledProcessError as exception:
        _LOGGER.fatal(exception.stderr)
        raise
    return completedprocess_gitdescribe.stdout.strip()


def _run_bpftrace(uid: int, gid: int, path_dir_pttcrun: Path, path_trees: Path, path_file_datalog: Path) -> None:
    _LOGGER.info("Starting `bpftrace` ...")
    # _getsetlimits()
    # TODO: parameterize path
    # TODO: avoid ulimit
    with path_file_datalog.open("wb") as file_datalog:
        popen_bpftrace = Popen(
            [  # noqa: S603 S607
                # TODO: Simplify
                # "sudo",
                # "-E",
                # "-n",
                # "sh",
                # "-c",
                # "ulimit -n 1048576; "
                "bpftrace",
                "-q",
                f"{path_trees.absolute()}",
            ],
            # TODO: Calculate BPFTRACE_MAX_PROBES
            close_fds=True,
            env={"BPFTRACE_MAX_PROBES": "1200", "BPFTRACE_STRLEN": "128"},
            stdout=file_datalog,
            stdin=DEVNULL,
            # TODO: Path
            cwd=PATH_POWERCAP_RAPL,
        )
        _LOGGER.info("Started `bpftrace` with PID %d.", popen_bpftrace.pid)
        while True:
            sleep(1)
            if path_file_datalog.stat().st_size > 0 or popen_bpftrace.poll() is not None:
                if popen_bpftrace.returncode is not None and popen_bpftrace.returncode > 0:
                    _LOGGER.error(
                        "`bpftrace` failed with exit status %d.",
                        popen_bpftrace.returncode,
                    )
                    raise RuntimeError()
                break
        _LOGGER.info("Intialized `bpftrace`.")
        # `bpftrace` is initialized
        # for path_executable in paths_executables:
        command_cargotest = [
            "cargo",
            "nextest",
            "run",
            "--all-features",
            "--no-fail-fast",
            "--test-threads=1",
            "--",
        ]
        try:
            run(
                command_cargotest,  # noqa: S603
                check=True,
                cwd=path_dir_pttcrun,
                encoding="utf-8",
                group=gid,
                stdin=DEVNULL,
                text=True,
                user=uid,
            )
        except CalledProcessError as exception:
            _LOGGER.exception(
                "`%s` failed: %s",
                " ".join(command_cargotest),
                "" if exception.stdout is None else exception.stdout,
            )
        sleep(1)
        _LOGGER.info("Stopping `bpftrace` ...")
        popen_bpftrace.terminate()
        with popen_bpftrace:
            pass
        _LOGGER.info("Syncing `bpftrace` output to filesystem ...")
        file_datalog.flush()
        fsync(fd=file_datalog)
        file_datalog.close()


def _run_nextest_list(uid: int, gid: int, path_dir_pttcrun: Path) -> dict:
    _LOGGER.info("Starting `cargo nextest list` ...")
    command_nextestlist = [
        "cargo",
        "nextest",
        "list",
        "--all-features",
        # TODO: add `--frozen`` to all invocations
        "--message-format=json-pretty",
        "--workspace",
    ]
    path_file_output_nextest = path_dir_pttcrun / "output_nextest.json"
    with path_file_output_nextest.open(mode="w") as file_output_nextest:
        try:
            run(
                command_nextestlist,  # noqa: S603
                encoding="utf-8",
                check=True,
                cwd=path_dir_pttcrun,
                group=gid,
                stdin=DEVNULL,
                stdout=file_output_nextest,
                text=True,
                user=uid,
            )
        except CalledProcessError as exception:
            _LOGGER.fatal(exception.stderr)
            raise
        file_output_nextest.flush()
    with path_file_output_nextest.open(mode="rb") as file_output_nextest:
        output_nextest = json.load(file_output_nextest)
        _LOGGER.debug(json.dumps(output_nextest, indent=True))
    return output_nextest


def _generate_bpftrace(path_dir_pttcrun: Path, output_nextest: dict) -> tuple[Path, Path]:
    path_trees = path_dir_pttcrun / "tracer.bt"
    path_file_datalog = path_dir_pttcrun / "data.toml"
    with path_trees.open("wt", encoding="utf-8") as file_bpftracescript:
        bpftraceprelude = Template(
            """BEGIN {
    $header
}
END {
    print("]");
}
""",
        ).substitute({"header": _template_bpftrace_header()})
        file_bpftracescript.write(bpftraceprelude)
        mode = path_trees.stat().st_mode
        mode |= (mode & 0o444) >> 2
        path_trees.chmod(mode=mode, follow_symlinks=False)
        paths_executables = []
        for testsuite, values in output_nextest["rust-suites"].items():
            path_executable = Path(values["binary-path"])
            paths_executables.append(path_executable)
            crate = path_executable.stem.split(sep="-", maxsplit=1)[0]
            _LOGGER.info("Processing testsuite %s for crate %s ...", testsuite, crate)
            for testfunctionitempath in values["testcases"]:
                for returning in (False, True):
                    bpftracescript = _template_bpftrace(
                        crate=crate,
                        functionitempath=testfunctionitempath,
                        path_executable=str(path_executable.absolute()),
                        returning=returning,
                    )
                    file_bpftracescript.write(bpftracescript)
        file_bpftracescript.flush()
        fsync(fd=file_bpftracescript)
    return path_trees, path_file_datalog


def _create_global_heatmaps(path_dir_pttc: Path) -> None:
    experiment_to_crate_to_functionitempath_to_deltarecord: dict[str, dict[str, dict[str, dict[str, int]]]] = {}
    for path_dir_pttcrun in path_dir_pttc.iterdir():
        if path_dir_pttcrun.is_dir():
            path_file_crate_to_functionitempath_to_deltarecord = (
                path_dir_pttcrun / "crate_to_functionitempath_to_deltarecord.json"
            )
            with path_file_crate_to_functionitempath_to_deltarecord.open() as file_crate_to_functionitempath_to_deltarecord:
                crate_to_functionitempath_to_deltarecord = json.load(file_crate_to_functionitempath_to_deltarecord)
                # TODO: simplify: only add experiment as keys.
                for (
                    crate,
                    functionitempath_to_deltarecord,
                ) in crate_to_functionitempath_to_deltarecord.items():
                    for (
                        functionitempath,
                        deltarecord,
                    ) in functionitempath_to_deltarecord.items():
                        experiment_to_crate_to_functionitempath_to_deltarecord.setdefault(
                            path_dir_pttcrun.name,
                            {},
                        ).setdefault(crate, {})[functionitempath] = deltarecord
    x: dict[str, set[str]] = {}
    y: dict[str, set[str]] = {}
    crates: set[str] = set()
    for (
        experiment,
        crate_to_functionitempath_to_delta_energy,
    ) in experiment_to_crate_to_functionitempath_to_deltarecord.items():
        # TODO:
        for (
            crate,
            functionitempath_to_deltarecord,
        ) in crate_to_functionitempath_to_delta_energy.items():
            for functionitempath in functionitempath_to_deltarecord:
                y.setdefault(crate, set()).add(functionitempath)
            x.setdefault(crate, set()).add(experiment)
            crates.add(crate)
    for crate in crates:
        _LOGGER.info("Producing heatmap for crate %s ...", crate)
        datamatrix_crate: list[list[int | None]] = [[None for _ in range(len(x[crate]))] for _ in range(len(y[crate]))]
        text: list[list[str]] = [["" for _ in range(len(x[crate]))] for _ in range(len(y[crate]))]
        for index_column, crate_to_functionitempath_to_deltarecord in enumerate(
            experiment_to_crate_to_functionitempath_to_deltarecord.values(),
        ):
            for index_row, deltarecord in enumerate(crate_to_functionitempath_to_deltarecord[crate].values()):
                delta_energy = deltarecord["delta_energy"]
                delta_time = deltarecord["delta_time"]
                datamatrix_crate[index_row][index_column] = delta_energy
                text[index_row][index_column] = f"{delta_energy:,d}<br>({delta_time:,d} ns)".replace(",", " ")
        heatmap(
            crate=crate,
            data=datamatrix_crate,
            path_dir_pttc=path_dir_pttc,
            text=text,
            x=list(x[crate]),
            y=list(y[crate]),
        )


def main() -> None:
    PATH_DIR_PTTC = Path(environ["PATH_DIR_PTTC"])
    path_dir_target = PATH_DIR_PTTC / "target"
    path_dir_target.mkdir(exist_ok=True)
    stat_dir_parent = path_dir_target.parent.stat()
    chown(path=path_dir_target, uid=stat_dir_parent.st_uid, gid=stat_dir_parent.st_gid)
    path_dir_pttc = path_dir_target / "pttc"
    path_dir_pttc.mkdir(exist_ok=True)
    chown(path=path_dir_pttc, uid=stat_dir_parent.st_uid, gid=stat_dir_parent.st_gid)
    _analyze(path_dir_pttc=path_dir_pttc, uid=stat_dir_parent.st_uid, gid=stat_dir_parent.st_gid)
    _create_global_heatmaps(path_dir_pttc=path_dir_pttc)


if __name__ == "__main__":
    main()
