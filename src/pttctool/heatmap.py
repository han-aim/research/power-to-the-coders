from pathlib import Path
from tempfile import mkstemp

import plotly.graph_objects as go


def heatmap(
    path_dir_pttc: Path,
    x: list[str],
    y: list[str],
    data: list[list[int]],
    text: list[list[str]],
    crate: str,
) -> None:
    _, str_path_file_heatmap = mkstemp(
        dir=path_dir_pttc,
        prefix=f"{crate} ",
        suffix=".html",
    )
    path_file_heatmap = Path(str_path_file_heatmap)
    autorange = True
    title = f"Energy trend per test ({crate})"
    layout = {
        "autosize": False,
        "height": 2000,
        "plot_bgcolor": "white",
        "title": title,
        "xaxis": {
            "autorange": autorange,
            "constrain": "domain",
            "title": {
                "text": "Commit⎵Trial ID",
            },
            "type": "category",
        },
        "yaxis": {
            "autorange": autorange,
            "exponentformat": "e",
            "constrain": "domain",
            "title": {"text": "Test function"},
            "type": "category",
        },
        "width": 1024,
    }
    go_heatmap = go.Heatmap(
        colorbar={
            "exponentformat": "e",
            "title": "Energy used (Δ µJ)",
        },
        colorscale="Jet",
        text=text,
        texttemplate="%{text}",
        textfont={"size": 18},
        x=x,
        xgap=1,
        y=y,
        ygap=1,
        z=data,
        zmin=0,
    )
    figure = go.Figure(data=go_heatmap, layout=layout)
    figure.update_xaxes(side="top")
    with path_file_heatmap.open(mode="wt", encoding="utf-8") as file_heatmap:
        file_heatmap.write(figure.to_html())
