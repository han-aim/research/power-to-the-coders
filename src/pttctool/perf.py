from collections.abc import MutableSet
from logging import INFO, basicConfig, getLogger
from pprint import pformat
from subprocess import CalledProcessError, run

_LOGGER = getLogger(__name__)
FORMAT = "%(levelname)s %(asctime)s %(process)d %(module)s %(funcName)s() %(message)s"
basicConfig(level=INFO, format=FORMAT)


def extract_probe_perf(
    probes_perf_failed: MutableSet[str],
    probes_perf_succeeded: MutableSet[str],
    path_executable: str,
    crate: str,
    testfunctionitempath: str,
    shorthand_probe: str,
) -> None:
    # TODO: Don't delete all probes, only those created through this script. Requires keeping state.
    # We cannot deal with e.g., overabundance of other uprobes anyway.
    try:
        run(["perf", "probe", "--del=*"], capture_output=True, check=True)  # noqa: S603 S607
    except CalledProcessError as exception:
        _LOGGER.warning(exception.stderr.decode("utf-8").strip())
    testfunctionitempath_escaped = testfunctionitempath.replace(":", "\\:")
    probespecification_perf = f"{shorthand_probe}={crate}\\:\\:{testfunctionitempath_escaped}"
    for modifier in ("", "%return"):
        try:
            completedprocess_perfprobeadd = run(
                [  # noqa: S603 S607
                    "perf",
                    "probe",
                    "--demangle",
                    "--exec",
                    path_executable,
                    f"--add={probespecification_perf}{modifier}",
                ],
                capture_output=True,
                check=True,
            )
        except CalledProcessError as exception:
            _LOGGER.exception(
                "`%s` failed with exit status %d: %s",
                " ".join(exception.cmd),
                exception.returncode,
                exception.stderr.decode("utf-8") + " " + exception.stdout.decode("utf-8"),
            )
            probes_perf_failed.add(probespecification_perf)
        else:
            probes_perf_succeeded.add(probespecification_perf)
            _LOGGER.debug(
                "%s",
                completedprocess_perfprobeadd.stderr.decode("utf-8").strip()
                + "\n"
                + completedprocess_perfprobeadd.stdout.decode("utf-8").strip(),
            )
    _LOGGER.debug(
        "`perf` probe additions succeeded: %d; failed: %d.",
        len(probes_perf_succeeded),
        len(probes_perf_failed),
    )
    _LOGGER.info(
        "`probes_perf_succeeded`: %s;\n`probes_perf_failed`: %s.",
        pformat(probes_perf_succeeded),
        pformat(probes_perf_failed),
    )
